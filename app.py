import sys


def run(argv):
    passengers = int(argv[0])
    if passengers > 30:
        sys.exit('Too many passengers! Tram can only take max 30')
    print('\nNumber of passengers: {}'.format(passengers))
    taken_seats = get_taken_seats(passengers)
    print_tram(taken_seats)


def print_tram(taken_seats):
    for x in range(30):
        sign = ''
        if x in taken_seats:
            sign = '*'
        print('[{}]'.format(sign), end='')


def get_taken_seats(passengers):
    left = 0
    right = 29
    taken = []
    for x in range(passengers):
        if x % 2 == 0 or x == 29:
            taken.append(left)
            left = get_next_left(left)
        else:
            taken.append(right)
            right = get_next_right(right)
    return taken


def get_next_left(idx):
    if idx < 14:
        return idx + 2
    else:
        return 15 - idx % 15


def get_next_right(idx):
    if idx > 17:
        return idx - 2
    else:
        return 29 - idx % 15 + 1


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit('Argument missing!')
    run(sys.argv[1:])
