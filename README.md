# Summary
Simple script to calculate seats taken in a tram considering two conditions:
```bash
People like to seat near the door
People don't like to seat next to other people if possible
```

# Usage
```bash
python app.py n
or
python lazy.py n
```
n - number of passengers

# Synopsis
First script takes an argument (number of passengers) from command line. Then it calculates the order of seats taken. This solution won't compute the values for trams with capacity of more than 30 seats, because of some hardcoded values. I've tried to be as generic in the solution as I could (for up to 30 seats), but there is a dirty condition check for the full tram.
Since the task has only had a 30 seater tram and no plans on buying bigger one, I've written a lazy solution too, where nothing is calculated but answers are returned one by one from a static list.