import sys

from app import print_tram


RESULT = [
    0, 29, 2, 27, 4, 25, 6, 23, 8, 21, 10, 19, 12, 17, 14,
    28, 1, 26, 3, 24, 5, 22, 7, 20, 9, 18, 11, 16, 13, 15
]


def run(argv):
    passengers = int(argv[0])
    if passengers > 30:
        sys.exit('Too many passengers! Tram can only take max 30')
    print('\nNumber of passengers: {}'.format(passengers))
    taken_seats = get_taken_seats(passengers)
    print_tram(taken_seats)


def get_taken_seats(passengers):
    return RESULT[:passengers]


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit('Argument missing!')
    run(sys.argv[1:])
